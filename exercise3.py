from flask import Flask
app = Flask(__name__)

@app.route('/')
def index():
    return '<h1> Hello World!</h1>'

@app.route('/contact-us')
def info():
   return "<h2> Contact US</h2>"

@app.route('/users/<username>')
def userprofile(username):
     return "<h3> This is a profile page for {}</h3>".format(username)

@app.route('/users/upper/<username>')
def userprofileupper(username):
     return "<h3>upper case{}</h3>".format(username.upper())

@app.route('/users/fifth/<username>')
def userprofilefifth(username):
     return "5th character of your name is ={}".format(username[5])

if __name__=='__main__':
    app.run(debug=True)